/*******************************************************************************
 * Copyright (c) 2017 Raphael Geissler. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: Raphael Geissler - initial API and implementation
 ******************************************************************************/

/**
 * name : Create Hello World Java Project
 */
loadModule("/System/UI")
include('https://gitlab.com/rage5474/eclipse-ease-script-library/raw/master/src/de.ragedev.eclipse.ease.script.api/helper/FormatJavaCompilationUnit.js')
include('https://gitlab.com/rage5474/eclipse-ease-script-library/raw/master/src/de.ragedev.eclipse.ease.script.api/helper/JavaSourceCodeHelper.js')

projectName = "HelloWorld"
packageName = "helloworld"

if(!getProject(projectName).exists()) {
	javaProject = createJavaProject(projectName)
	sourceFolder = javaProject.getProject().getFolder("src")
	pack = createJavaPackage(javaProject, sourceFolder, packageName)
	helloWorldSource = "public class HelloWorld { public static void main(String[] args) { System.out.println(\"Hello World!\"); } }"
	helloWorldClass = createJavaClass(pack, "HelloWorld.java", helloWorldSource)
	formatUnitSourceCode(helloWorldClass)	
}
else {
	showErrorDialog("Could not create " + projectName + " project, because a project with the name " + projectName + " already exists.")
}
