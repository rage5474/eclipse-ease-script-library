/*******************************************************************************
 * Copyright (c) 2017 Raphael Geissler. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: Raphael Geissler - initial API and implementation
 ******************************************************************************/

loadModule("/System/Resources")

function createOrGetProject(name)
{
	var project = getProject(name)
	
	if(!project.exists())
		project = createProject(name)
		
	return project
}

function createJavaProject(name)
{
	project = createProject(name)
	project.open(null)
	description = project.getDescription()
	description.setNatureIds([org.eclipse.jdt.core.JavaCore.NATURE_ID]);
	project.setDescription(description, null);
	
	javaProject = org.eclipse.jdt.core.JavaCore.create(project); 
	
	binFolder = project.getFolder("bin")
	binFolder.create(false, true, null)
	javaProject.setOutputLocation(binFolder.getFullPath(), null)
	
	vmInstall = org.eclipse.jdt.launching.JavaRuntime.getDefaultVMInstall()

	locations = org.eclipse.jdt.launching.JavaRuntime.getLibraryLocations(vmInstall)
	
	entries = new Array()
	
	for (i = 0; i < locations.length; i++) {
		entries[i] = org.eclipse.jdt.core.JavaCore.newLibraryEntry(locations[i].getSystemLibraryPath(), null, null)
	}
	
	javaProject.setRawClasspath(entries, null);
	
	sourceFolder = project.getFolder("src");
	sourceFolder.create(false, true, null);
	
	root = javaProject.getPackageFragmentRoot(sourceFolder);
	oldEntries = javaProject.getRawClasspath();
	newEntries = oldEntries.slice();
	
	newEntries[oldEntries.length] = org.eclipse.jdt.core.JavaCore.newSourceEntry(root.getPath())
	javaProject.setRawClasspath(newEntries, null)
	
	return javaProject
}

function createJavaPackage(javaProject, sourceFolder, packageName)
{
	return javaProject.getPackageFragmentRoot(sourceFolder).createPackageFragment(packageName, false, null);
}

function createJavaClass(javaPackage, className, content)
{
	buffer = "package " + javaPackage.getElementName() + ";\n"
	buffer += "\n"
	buffer += content
	
	return pack.createCompilationUnit(className, buffer, false, null);
}