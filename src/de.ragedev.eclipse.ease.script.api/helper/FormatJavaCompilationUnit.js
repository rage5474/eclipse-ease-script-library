/*******************************************************************************
 * Copyright (c) 2015 - 2017 Christian Pontesegger and others. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: Christian Pontesegger - initial API and implementation
 * 				 Raphael Geissler - changed implementation
 * 
 * unit : Formats ICompilationUnit : Applies the current Java code
 * formatter settings to a given source file.
 ******************************************************************************/

function formatUnitSourceCode(unit) {

	unit.becomeWorkingCopy(null);

	options = new java.util.Hashtable()
	options.put(org.eclipse.jdt.core.JavaCore.COMPILER_SOURCE,"1.8")
	options.put(org.eclipse.jdt.core.JavaCore.COMPILER_COMPLIANCE,"1.8")
	options.put(org.eclipse.jdt.core.JavaCore.COMPILER_CODEGEN_TARGET_PLATFORM,
			"1.8")

	formatter = org.eclipse.jdt.core.ToolFactory.createCodeFormatter(options);
	range = unit.getSourceRange();
	formatEdit = formatter
			.format(
					org.eclipse.jdt.core.formatter.CodeFormatter.K_COMPILATION_UNIT
							| org.eclipse.jdt.core.formatter.CodeFormatter.F_INCLUDE_COMMENTS,
					unit.getSource(), 0, unit.getSource().length(), 0, null);
	if (formatEdit.hasChildren()) {
		unit.applyTextEdit(formatEdit, null);
		unit.reconcile(org.eclipse.jdt.core.dom.AST.JLS4, false, null, null);
	}

	unit.commitWorkingCopy(true, null);
}
